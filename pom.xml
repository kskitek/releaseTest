<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.ks.misc</groupId>
    <artifactId>release</artifactId>
    <version>1.2-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <sentry.appender><![CDATA[<appender-ref ref="Sentry" level="WARN" />]]></sentry.appender>

        <version.junit>4.12</version.junit>
        <version.mockito>2.16.0</version.mockito>
        <version.sentry>1.7.2</version.sentry>
        <version.slf4j>1.7.25</version.slf4j>
        <version.log4j2>2.11.0</version.log4j2>
    </properties>

    <dependencies>

        <!-- logging -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${version.slf4j}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>${version.log4j2}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${version.log4j2}</version>
            <!--<type>runtime</type> TODO why runtime does not work?! -->
        </dependency>

        <dependency>
            <groupId>io.sentry</groupId>
            <artifactId>sentry-log4j2</artifactId>
            <version>${version.sentry}</version>
            <scope>runtime</scope>
            <!-- 'runtime' helps to keep dev classpath clean; when using Sentry context programmatically remove it -->
        </dependency>

        <!-- testing -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${version.junit}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${version.mockito}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>

        <profile>
            <id>dev</id>
            <!-- sets development environment properties -->
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <environment>deveploment</environment>
                <sentry.appender/>
            </properties>
        </profile>

        <profile>
            <id>test</id>
            <!-- sets test environment properties -->
            <properties>
                <environment>test</environment>
            </properties>
        </profile>

        <profile>
            <id>ci-checks</id>
            <!-- provides additional checks that CI can do-->
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>findbugs-maven-plugin</artifactId>
                        <version>3.0.5</version>
                        <executions>
                            <execution>
                                <id>analyze-compile</id>
                                <phase>compile</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.owasp</groupId>
                        <artifactId>dependency-check-maven</artifactId>
                        <version>3.1.1</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                </plugins>
            </build>
        </profile>

        <profile>
            <id>deployment</id>
            <!-- use when deploying to always deploy full definition of project -->
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.0.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar</goal>
                                    <goal>test-jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.0.0</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

    </profiles>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
            </plugin>
        </plugins>
    </build>

    <developers>
        <developer>
            <name>Krzysztof Skitek</name>
            <email>krzysztof.skitek@gmail.com</email>
        </developer>
    </developers>

    <scm>
        <connection>https://gitlab.com/kskitek/releaseTest.git</connection>
        <developerConnection>scm:git:git@gitlab.com:kskitek/releaseTest.git</developerConnection>
        <url>https://gitlab.com/kskitek/releaseTest</url>
        <tag>HEAD</tag>
    </scm>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>terefere-releases</id>
            <url>http://46.101.212.137:8081/repository/terefere-releases/</url>
        </repository>
        <repository>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <id>terefere-snapshots</id>
            <url>http://46.101.212.137:8081/repository/terefere-snapshots/</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>terefere</id>
            <url>http://46.101.212.137:8081/repository/terefere-releases/</url>
        </repository>
        <snapshotRepository>
            <id>terefere</id>
            <url>http://46.101.212.137:8081/repository/terefere-snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

</project>